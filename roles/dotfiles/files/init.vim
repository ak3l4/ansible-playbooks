"Basic settings"
set number relativenumber
autocmd BufWritePre * %s/\s\+$//e
set mouse=a
syntax on

"Tab Settings"
set shiftwidth=4
set softtabstop=4
set tabstop=4

"setup cursor install"
set cursorline
highlight cursorline cterm=bold

"Plugins"
call plug#begin()
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'preservim/nerdtree'
call plug#end()

"Themeing"
set termguicolors
colorscheme dracula

" Fix splits"
set splitbelow splitright

"Auto complettion"
set wildmode=longest,list,full

"Better Splits"
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

